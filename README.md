# standards-silks-and-hoop

This is a test project of GitLab pages.

## Open a website

Go to the link: https://lessy09.gitlab.io/standards-silks-and-hoop/

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/lessy09/standards-level-one.git
git branch -M main
git push -uf origin main
```

## TODOs

- Add description to the main page

### Useful links
- https://github.com/tonyflo/tonyflo.github.io/blob/main/elements.html
- https://tonyflo.github.io/elements.html
